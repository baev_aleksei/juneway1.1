# Блог

Это просто тестовый блок на Django

### Как установить

Docker, docker-compose должен быть уже установлен. 
В корне проекта должен лежать db.env с переменными подключения к БД Postgress. Имя и расположение файла можно переопределить в docker-compose.yml, директива:
```
env_file:
      - ./db.env
```

Переменные:
```
POSTGRES_DB=postgres
POSTGRES_USER=<Пользователь>
POSTGRES_PASSWORD=<Пароль>
```

По умолчанию:
```
POSTGRES_DB=postgres
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
```

Затем используйте docker-compose build в папке проекта  для сборки:
```
docker-compose build
```

### Как запустить

Выполнить запуск:
```
docker-compose up -d
```